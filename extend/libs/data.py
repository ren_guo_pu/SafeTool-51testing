#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
PATHS_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../")
PATHS_PLUGIN = os.path.join(PATHS_ROOT,"plugin")
PATHS_REGISTER = os.path.join(PATHS_ROOT,"register")
PATHS_CONF = os.path.join(PATHS_ROOT,"CONF")
PLUGIN = []
CONF = {
    "plugin" :"",
    "args" : "",
    "kwargs":{},
}
VERSION = "v0.1"
ROOT_NAME = "extend://"