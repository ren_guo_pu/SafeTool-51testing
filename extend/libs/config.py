#!/usr/bin/env python
# -*- coding:utf-8 -*-
import yaml
import os
import re
from libs.data import PATHS_CONF
class PluginsConfig:
    def __init__(self):
        self.yamlPath = PATHS_CONF + os.path.sep + "plugins.yaml"
        rFile = open(self.yamlPath,'r',encoding='utf-8')
        content = rFile.read()
        self.conf = yaml.load(content,Loader=yaml.FullLoader)
    def get_plugins(self):
        keys = []
        for k in self.conf.keys():
            keys.append(k)
        return keys
    def get_info(self,plugin):
        return self.conf[plugin]
    def set_plugin(self,plugins:dict):
        self.conf.update(plugins)
        wFile = open(self.yamlPath,'w',encoding='utf-8')
        yaml.dump(self.conf,wFile)         
    