#!/usr/bin/env python
# -*- coding:utf-8 -*-

def is_matching(status,content,hc=[],ht=None,st=None,sc=[]):
    """
    @des
    对返回内容进行匹配
    @params
    status : 返回对象的状态码 http
    content: 返回对象的内容
    hc : 返回对象的状态码存在此列表中，即无效，则隐藏
    ht : 响应内容存在此值，即无效，则隐藏
    sc : 返回对象的状态码存在此列表中，即有效，则显示
    st : 响应内容存在此值，即有效，则显示
    """
    try:
        if ht is not None:
            if ht in content:
                return False
        if st is not None:
            if st in content:
                return True
        if len(hc)>0:
            if str(status) in hc:
                return False
            else:
                return True
        if len(sc) > 0:
            if str(status) in sc:
                return True
            else:
                return False
    except Exception as e:
        raise ValueError("is_matching 错误!")
    return True
def replace_tag(d,tag,word):
    """
    @des
    使用指定的标记，替换键和值
    @params
    d: 字典值
    tag : 用于替换的标记
    word : 替换标记的值
    """
    for k,v in d.items():
        n_k = k.replace(tag,word)
        n_v = v.replace(tag,word)
        d[n_k] = n_v
        if n_k != k:
            del d[k]
    return d

def replace_words(d,tag,words=[]):
    if d and words:
        for w in words:
            d = replace_tag(d,w,tag)
    return d

def judge_tag(content,tag):
    if tag in content:
        return True
    return False

