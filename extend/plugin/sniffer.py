#!/usr/bin/env python
# -*- coding:utf-8 -*-

from libs import printer
import scapy.all as scapy
from scapy_http import http
import traceback
keywords = []
def sensitive_keyword():
    result = []
    keywords = input("请输入数据提取的关键字(多个用逗号隔开): ")
    if "," in keywords:
        result = keywords.split(",")
    return result
def extract_packets_http(packet:scapy.packet.Packet):
    global keywords
    if packet.haslayer(http.HTTPRequest):
        url = packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path
        if packet.haslayer(scapy.Raw):
            info = str(packet[scapy.Raw].load,encoding="utf-8")
            if keywords:
                for k in keywords:
                    if k in info:
                        printer.info("发现可疑的敏感数据: " + info)
def execute(args):
    try:
        global keywords
        printer.info("当前可用的网卡信息: ")
        scapy.show_interfaces()
        idex = input("请输入要监听的网卡标号(index列): ")
        idex = idex.strip()
        if idex:
            keywords = sensitive_keyword()
            scapy.sniff(iface = scapy.IFACES.dev_from_index(int(idex)),prn = extract_packets_http, store = 0,count = 0)
        else:
            printer.warn("输入错误!")
    except KeyboardInterrupt as k:
        printer.info("退出！")
    except Exception as e:
        traceback.print_exc()
