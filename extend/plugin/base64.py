#!/usr/bin/env python
# -*- coding:utf-8 -*-
import binascii
from libs import printer

def base64_encoder(cstr):
    text = binascii.b2a_base64(str.encode(cstr))
    return text

def base64_decoder(cstr):
    text = binascii.a2b_base64(cstr)
    return text
def base64_menu():
    printer.info("Base64选项: ")
    printer.plus("1. Base64编码")
    printer.plus("2. Base64解码")
  
def execute(args):
    try:
        base64_menu()
        choise = input("请选择: ")
        if choise.strip() == "1":
            cstr = input("请输入编码内容: ")
            cstr = cstr.strip()
            printer.info("结果如下: ")
            print(base64_encoder(cstr))
        elif choise.strip() == "2":
            cstr = input("请输入解码内容: ")
            cstr = cstr.strip()
            printer.info("结果如下: ")
            print(base64_decoder(cstr))
        else:
            printer.warn("选项不存在!")           
    except Exception as e:
        print(e)