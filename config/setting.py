#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

#目录配置
PROJRCT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../")#程序主目录
CONF_PATH = os.path.join(PROJRCT_PATH,"config")#配置文件目录
CService_PATH = os.path.join(PROJRCT_PATH,"controlService")#控制服务目录
DB_PATH = os.path.join(PROJRCT_PATH,"db")#数据库目录
DECORATORS_PATH = os.path.join(PROJRCT_PATH,"decorators")#装饰器目录
DICT_PATH = os.path.join(PROJRCT_PATH,"dict")#字典目录
EXTEND_PATH = os.path.join(PROJRCT_PATH,"extend")#扩展平台目录
FSERVER_PATH = os.path.join(PROJRCT_PATH,"forwardServer")#web服务目录,实现了简单的url路径映射,可做为模拟网站使用
LSERVICE_PATH = os.path.join(PROJRCT_PATH,"listenService")#监听服务，配合forwardServer
STAPI_PATH =  os.path.join(PROJRCT_PATH,"samllToolApi")#小工具用到的函数
SCAN_PATH = os.path.join(PROJRCT_PATH,"scan")#扫描平台
POC_PATH = os.path.join(PROJRCT_PATH,"spigpoc")#poc平台
UTILS_PATH = os.path.join(PROJRCT_PATH,"utils")#工具函数

##decorators子目录
MFILE_PATH = os.path.join(DECORATORS_PATH,"multipartfile")#保存multipart的参数信息



