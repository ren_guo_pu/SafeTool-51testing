#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
def get_files(fDir,suf):
    '''
    @des
    获取指定目录下指定文件格式的文件名
    suf为空，则获取完整文件名
    '''
    result = []
    for _,_,files in os.walk(fDir):
        for f in files:
            name,suffix = os.path.splitext(f)
            if suf:
                if suffix == suf:
                    result.append(name)
            else:
                result.append(f)
    return result

