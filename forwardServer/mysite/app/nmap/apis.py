#!/usr/bin/env python
# -*- coding:utf-8 -*-
from http.server import BaseHTTPRequestHandler
import yaml
import os,sys
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) #当前程序上上上一级目录
sys.path.append(BASE_DIR) #添加环境变量
from utils.sendSocket import sendTcpData
from utils.useCmd import cmdExec,cmdExecCom
from app.nmap.config import config
import _thread as thread
def nmap_help(httpserver:BaseHTTPRequestHandler,params):
    hs = httpserver
    resp = {

        'result':"success",
        'params':params
    }
    hs.send_json_success(resp)
    cmd = 'nmap -help'
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))

def nmap_ping(httpserver:BaseHTTPRequestHandler,params):
    '''
    ping扫描,nmap Ip [防火墙打开,无法扫描]
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -sP " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_ping_range(httpserver:BaseHTTPRequestHandler,params):
    '''
    指定范围ping扫描,扫描一个网段下的ip,nmap Ip [防火墙打开,无法扫描]
    @params IP 10.1.1.0 /24
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -sP " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_ping_not_port(httpserver:BaseHTTPRequestHandler,params):
    '''
    ping探测扫描主机， 不进行端口扫描 ,即使目标主机丢弃icmp包,依然可以检测到开机状态
    @params IP 10.1.1.0
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -sn " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,)) 
def nmap_ack(httpserver:BaseHTTPRequestHandler,params):
    '''
    tcp 发送ack包进行探测,探测主机是否存活
    @params IP 10.1.1.0
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -sA " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))         
def nmap_traceroute(httpserver:BaseHTTPRequestHandler,params):
    '''
    路由跟踪
    params:IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -traceroute " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))    
def nmap_syn(httpserver:BaseHTTPRequestHandler,params):
    '''
    半开放扫描
    syn 扫描 发送syn包,返回确认rst包或者syn/ack
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -sS " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_tcp(httpserver:BaseHTTPRequestHandler,params):
    '''
    完整的tcp扫描,3次握手,默认的扫描方式
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -sT " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_udp(httpserver:BaseHTTPRequestHandler,params):
    '''
    udp端口扫描,发送udp数据包,扫描速度相对慢
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -sU " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_fin(httpserver:BaseHTTPRequestHandler,params):
    '''
    tcp扫描,发送FIN标志的数据包,绕过IDS/IPS
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -sF " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_xmasTree(httpserver:BaseHTTPRequestHandler,params):
    '''
    圣诞树扫描,发送FIN,URG,PUSH标志位的数据包,用于绕过一般的IDS/IPS
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -sX " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_null(httpserver:BaseHTTPRequestHandler,params):
    '''
    空扫描,用于绕过一般的IDS/IPS
    @params IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -sN " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
            
def nmap_set_port_range(httpserver:BaseHTTPRequestHandler,params):
    '''
    自定义设置端口范围
    params['port'] @ex:50-80
    params['ip]
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -p" + params['port'] +" "+params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_specify_port_range(httpserver:BaseHTTPRequestHandler,params):
    '''
    指定端口范围
    params['port'] @ex:80,81,82
    params['ip]    
    '''
    hs = httpserver
    resp = {
            'result':'success!'
        }
    hs.send_json_success(resp)
    cmd = "nmap -p" + params['port'] +" "+params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_os_version(httpserver:BaseHTTPRequestHandler,params):
    '''
    操作系统类型探测
    params:IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -O " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_all_scan(httpserver:BaseHTTPRequestHandler,params):
    '''
    全能探测 1-10000端口ping扫描,操作系统扫描,脚本扫描,路由扫描,服务探测等
    params:IP
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = "nmap -A " + params['ip']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def nmap_custom(httpserver:BaseHTTPRequestHandler,params):
    '''
    自定义命令扫描
    params:cmd
    '''
    hs = httpserver
    resp = {
            'result':'success!'
            }
    hs.send_json_success(resp)
    cmd = params['cmd']
    print("order: " + cmd)
    thread.start_new_thread(nmap_cmd,(cmd,))
def cmd_config():
    conf = config()
    ip = conf.get_ip()
    port = conf.get_port()
    return ip,port                    
def nmap_cmd(cmd):
    ip,port = cmd_config()
    for c in cmdExec(cmd):
        sendTcpData(ip,port,c)
