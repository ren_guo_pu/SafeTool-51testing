#!/usr/bin/env python
# -*- coding:utf-8 -*-
from http.server import BaseHTTPRequestHandler
import yaml
import os
def xsstester(httpserver:BaseHTTPRequestHandler,params):
    htmlpath = "template\\xsstester.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + htmlpath
    f = open(fp,"rb")
    resp = f.read()
    f.close()
    httpserver.send_html_success(resp)

