#!/usr/bin/env python
# -*- coding:utf-8 -*-

from urllib.error import HTTPError

class SpigScanUnboundLocalError(UnboundLocalError):
    pass

class SpigScanDataException(Exception):
    pass

class SpigScanNoneException(Exception):
    pass

class SpigScanInputException(Exception):
    pass

class SpigScanGenericException(Exception):
    pass

class SpigScanConnectionException(HTTPError):
    pass

class SpigScanKeyboardInterrupt(KeyboardInterrupt):
    pass

