#!/usr/bin/env python
# -*- coding:utf-8 -*-

from lib.utils.check import *
from lib.utils.printer import *
from lib.request.request import *

class apache(Request):
    """
    Apache服务器默认配置检测
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'apache':None
        }

    def check(self):
        info("检测Apache服务器的各状态页面:")
        paths = ['perl-status','server-status','server-info',
        'stronghold-info','stronghold-status']
        isNothing = True
        splitUrl = SplitUrl(self.url)
        netlocUrl = splitUrl.netloc
        for path in paths:
            url = Cpath(netlocUrl,path)
            more("检测载荷:{}".format(url))
            req = self.Send(url=url,method=self.get)
            if req.code == 200:
                if CEndUrl(req.url) == url:
                    plus("疑似存在Apache相关的{}模块启用:{}".format(path,req.url))
                    if self.result['apache']:
                        self.result['apache'] = path
                    else:
                        self.result['apache'] += " ," + path
                    isNothing = False
        if isNothing:
            info_nothing()
        return self.result

def run(kwargs,url,data):
    result = {}
    scan = apache(kwargs,url,data)
    result = scan.check()
    return result
