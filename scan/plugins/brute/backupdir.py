#!/usr/bin/env python 
# -*- coding:utf-8 -*-

from os import path
from queue import Queue
from threading import Thread
from lib.utils.check import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.readfile import *
from lib.utils.settings import T_MAX,BACKUPDIR_PATH

global_isNothing = True
global_result = []
class backupdir(Request):
    """
    检测可能的备份目录
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
    
    def check(self):
        info("检测可能的备份目录...")
        queue = Queue(T_MAX)
        for _ in range(T_MAX):
            thread = ThreadBrute(self.url,queue,self)
            thread.daemon = True
            thread.start()
        for path in readfile(BACKUPDIR_PATH):
            queue.put(path.decode("utf-8"))
        queue.join()

class ThreadBrute(Thread):
    get = "GET"
    #备份文件可能用到的扩展名
    EXT = ['.zip', '1', '2', '.bak', '_old', '_bak', '.tar.gz', '.tgz']

    def __init__(self,target,queue:Queue,request:Request):
        Thread.__init__(self)
        self.queue = queue
        self.target = target
        self.request = request

    def check(self):
        global global_isNothing
        global global_result
        while True:
            try:
                path = self.queue.get()
                for ext in self.EXT:
                    _path_ = "%s%s"%(path,ext)
                    splitUrl = SplitUrl(self.target)
                    netlocUrl = splitUrl.netloc
                    url = Cpath(netlocUrl,_path_)
                    more("检测载荷:{}".format(url))
                    req = self.request.Send(url=url,method=self.get)
                    if req.code == 200:
                        if CEndUrl(req.url) == url:
                            global_result.append(req.url)
                            global_isNothing = False
                self.queue.task_done()
            except Exception as e:
                print(e)
    def run(self):
        self.check()
def run(kwargs,url,data):
    result = {
        "backupdir":None
    }

    scan = backupdir(kwargs,url,data)
    scan.check()
    if global_isNothing:
        info_nothing()
    else:
        if global_result:
            for r in global_result:
                plus('疑似找到备份目录: {}'.format(r))
                if not result['backupdir']:
                    result['backupdir'] = r
                else:
                    result['backupdir'] += ' ,' + r
    return result


