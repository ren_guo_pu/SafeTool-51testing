#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import queue
PATHS_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../../")
PATHS_POCS = os.path.join(PATHS_ROOT,"pocs")
PATHS_OUTPUT = os.path.join(PATHS_ROOT,"output")
PATHS_LIBS = os.path.join(PATHS_ROOT,"libs")
LIBS_CONFIG = os.path.join(PATHS_LIBS,"config")
LIBS_DB = os.path.join(PATHS_LIBS,"db")
DB_CONF = os.path.join(LIBS_DB,"conf")
DB_CONF_POC = DB_CONF + os.sep + "pocsinfo.yaml"
POCS = []
CONF = {
            "url" : "",
            "thread_num" : 2,
            "requests":{
                "timeout":10,
                "headers":{
                }
            },
            "poc" : [],
            "method":"verify",
            "args":{}
}
WORKER = queue.Queue()
VERSION = "v0.1"
ROOT_NAME = "spigpoc://"
