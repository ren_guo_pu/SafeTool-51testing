#!/usr/bin/env python
# -*- coding:utf-8 -*-

from re import sub,I,findall
import libs.utils.colorForUnix as cu 
import libs.utils.colorForWin as cw 
from urllib.parse import urlsplit,urljoin
from libs.utils.rand import r_string
from libs.utils.printer import *
import platform

def Cplatform():
    op = platform.system()
    if op == 'Windows':
        v = 'win'
    else:
        v = 'other'
    return v
def Cpath(url,path):
    """
    Args
        url地址
        访问的路径
    Return
        完整的url,url+path
    """
    p = urljoin(url,path)
    return p

def CrandParams(params):
    random_str = "%s"%(r_string(10)).upper()
    if '=' not in params:
        v = "%s=%s"%(params,random_str)
    else:
        v = "%s"%(r_string(10)).upper()
    return v

def Cquery(url:str,params):
    params = CrandParams(params)
    if url.endswith("?"):
        v = url + params
    elif not url.endswith('?'):
        if url.endswith("&"):
            v = url + params
        elif '?' in url and '&' not in url:
            v = url +'&' + params
        else:
            v = url + '?' + params
    else:
        v = url + "?" + params
    return v

def Cparams(url):
    op = Cplatform()
    if '&' not in url:
        if op == 'win':
            pass
        else:
            url = sub(findall(r'\?(\S*)\=',url)[0],'%s%s%s'%(GREEN%(1),findall(r'\?(\S*)\=',url)[0],RESET),url)
    else:
        if op == 'win':
            pass
        else:
            url = sub(findall(r'\&(\S*)\=',url)[0],'%s%s%s'%(GREEN%(1),findall(r'\&(\S*)\=',url)[0],RESET),url)
    return url

def Curl(url):
    split = urlsplit(url)
    scheme = split.scheme
    if  scheme not in ['http','https','']:
        exit(less('协议错误, "%s" 协议不支持!'%(split.scheme)))
    else:
        if scheme not in ['http','https']:
            v = "http://%s"%(url)
        else:
            v = url
        return v


def CNquery(url):
    if '?' in url:
        parse = urlsplit(url)
        if parse.scheme:
            v = parse.scheme + '://' + parse.netloc + '/'
        else:
            v = 'http://' + parse.path+'/'
    else:
        parse = urlsplit(url)
        if parse.scheme:
            v = parse.scheme + '://' + parse.netloc + '/'
        else:
            v = 'http://' + parse.path + '/'
    return v            

def CEndUrl(url):
    if url.endswith("/"):
        v = url[:-1]
        return v
    return url

def Cscan(scan):
    if scan not in ['1','2','3','4','5']:
        v = '目前只支持5个功能模块!默认执行全部!'
        info(v)
        scan = 5
    if isinstance(scan,str):
        return int(scan)
    return scan

class SplitUrl:
    def __init__(self,url):
        self.scheme = urlsplit(url).scheme
        self.netloc = Curl(urlsplit(url).netloc)
        self.path = urlsplit(url).path
        self.query = urlsplit(url).query
        self.fragment = urlsplit(url).fragment
        

def Cheaders(headers):
    _ = {}
    if ":" in headers:
        if ',' in headers:
            headerList = headers.split(',')
            for header in headerList:
                _[header.split(":")[0]] = header.split(":")[1]
        else:
            _[headers.split(":")[0]] = headers.split(":")[1]
    return _

def Cauth(auth):
    if ':' not in auth:
        v = "%s:"%(auth)
        return v
    return auth





