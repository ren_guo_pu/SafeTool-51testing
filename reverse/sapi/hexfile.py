#!/usr/bin/env python
# -*- coding:utf-8 -*-
from lib.utils import printer

class HexTool:
    def __init__(self,filename):
        self.columns = 16
        self.filename = filename
        self.file_content = b''
        self.hex_array = []
        self.hex_array_len = 0
        self.decode_array = []
        self.offset = []
        self.s_char = ["\a", "\b", "\f", "\n", "\r", "\t", "\v", "\x00"]
        self.hex_content = []
    
    def read_file(self):
        self.file_content = open(self.filename,"rb")
    
    def convert_hex(self):
        for line in self.file_content:
            for byte in line:
                self.hex_array_len += 1
                hex_byte = hex(byte).replace("x","").upper()
                if byte >= 16:
                    hex_byte = hex_byte.lstrip("0")
                self.hex_array.append(hex_byte)

    def format_content(self):
        line = []
        hex_array = []
        count = 0
        for byte in self.hex_array:
            if count == self.columns:
                hex_array.append(line)
                line = []
                count = 0
            line.append(byte)
            count += 1
        if line:
            hex_array.append(line)
        self.hex_array = hex_array
    
    def ascii_from_hex(self):
        decode_array  = []
        for line in self.hex_array:
            lines = []
            for h in line:
                b = bytes.fromhex(h)
                try:
                    ascii_code = b.decode("ascii")
                except Exception as e:
                    ascii_code = "."
                if ascii_code in self.s_char:
                    ascii_code = "."
                lines.append(ascii_code)
            decode_array.append(lines)
        self.decode_array = decode_array

    def calc_offset(self):
        for i in range(0,len(self.hex_array)):
            o = hex(i * self.columns).replace("x","").upper()
            o = "0" * (8 - len(str(o))) + str(o)
            self.offset.append(o)
    def convert_hexfile(self):
        self.read_file()
        self.convert_hex()
        self.format_content()

    def connect_content(self):
        line = ""
        for o,h,d in zip(self.offset,self.hex_array,self.decode_array):
            hlen = len(" ".join(h))
            if hlen < 47:
                diff = 47 - hlen -1
                line = o + ":" + " ".join(h) + diff * " "+"".join(d)
                self.hex_content.append(line)
            else:
                line = o + ":" + " ".join(h) +" "+"".join(d)
                self.hex_content.append(line)
    def show_hex_content(self):
        for o,h,d in zip(self.offset,self.hex_array,self.decode_array):
            hlen = len(" ".join(h))
            if hlen < 47:
                diff = 47-hlen + 1
                offset = o + ":"
                hexc = " ".join(h)
                decodeHex = diff * " "+"".join(d)
                printer.offset(offset)
                printer.hex(hexc)
                printer.ascii(decodeHex)
            else:
                offset = o + ":"
                hexc = " ".join(h)
                decodeHex = " "+"".join(d)
                printer.offset(offset)
                printer.hex(hexc)
                printer.ascii(decodeHex)
            print("")



