#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Binary:二进制
Octal:八进制 
Decimal:十进制
Hexadecimal:十六进制
'''
def decimal_binary(d):
    d = int(d)
    return bin(d)

def decimal_octal(d):
    d = int(d)
    return oct(d)

def decimal_hex(d):
    d = int(d)
    return hex(d)

def binary_octal(b):
    b = int(b,2)
    return decimal_octal(b)

def binary_decimal(b):
    return int(b,2)

def binary_hex(b):
    b = int(b,2)
    return hex(b)

def octal_binary(o):
    o = int(o,8)
    return bin(o)

def octal_decimal(o):
    return int(o,8)

def octal_hex(o):
    o = int(o,8)
    return hex(o)

def hex_binary(h):
    h = int(h,16)
    return bin(h)

def hex_octal(h):
    h = int(h,16)
    return oct(h)

def hex_decimal(h):
    return int(h,16)

def calc_va(imageBase,rva):
    imageBase = hex_decimal(imageBase)
    rva = hex_decimal(rva)
    va = imageBase + rva
    return decimal_hex(va)

def calc_imageBase(va,rva):
    va = hex_decimal(va)
    rva = hex_decimal(rva)
    imageBase = va - rva
    return decimal_hex(imageBase)

def calc_rva(va,imageBase):
    va = hex_decimal(va)
    imageBase = hex_decimal(imageBase)
    rva = va - imageBase
    return decimal_hex(rva)

#二进制计算
def binary_add(a,b):
    a = binary_decimal(a)
    b = binary_decimal(b)
    c = a+b
    return decimal_binary(c)
def binary_sub(a,b):
    a = binary_decimal(a)
    b = binary_decimal(b)
    c = a-b
    return decimal_binary(c)
def binary_mul(a,b):
    a = binary_decimal(a)
    b = binary_decimal(b)
    c = a*b
    return decimal_binary(c)
def binary_div(a,b):
    a = binary_decimal(a)
    b = binary_decimal(b)
    c = a/b
    return decimal_binary(c)
#十六进制计算
def hex_add(a,b):
    a = hex_decimal(a)
    b = hex_decimal(b)
    c = a + b
    return decimal_hex(c)
def hex_sub(a,b):
    a = hex_decimal(a)
    b = hex_decimal(b)
    c = a - b
    return decimal_hex(c)
def hex_mul(a,b):
    a = hex_decimal(a)
    b = hex_decimal(b)
    c = a * b
    return decimal_hex(c)
def hex_div(a,b):
    a = hex_decimal(a)
    b = hex_decimal(b)
    c = a / b
    return decimal_hex(c)






