#!/usr/bin/env python
# -*- coding:utf-8 -*-
import ctypes
import time
DBG_CONTINUE = 0x00010002

def debug_single_step64(debugObj:object):
    print("[*] 单步调试地址: 0x%08x" % debugObj.exception_address)
    if debugObj.context:
        debugObj.general_register_info64(debugObj.context)
        debugObj.eflags_register(debugObj.context)
        debugObj.flags_register_info64(debugObj.context)
        debugObj.segment_register_info64(debugObj.context)
        debugObj.context.EFlags |= 0x100
    ctypes.windll.kernel32.SetThreadContext(debugObj.h_thread,ctypes.byref(debugObj.context))
    time.sleep(2)
    return DBG_CONTINUE

def run(debugObj:object,flag):
    continue_status = DBG_CONTINUE
    if flag == 64:
        continue_status =debug_single_step64(debugObj)
    else:
        print("参数错误!")
    return continue_status