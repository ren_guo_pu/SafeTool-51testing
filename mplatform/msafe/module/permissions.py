#!/usr/bin/env python
# -*- coding:utf-8 -*-

from androguard.core.bytecodes.apk import APK
from madb.tools.printer import info,plus,warn

class Permissions(object):
    def __init__(self,apkPath):
        self.apk = APK(apkPath)
    def list_permissions(self):
        permissions = self.apk.get_permissions()
        plus("当前apk申请的权限有:")
        for permission in permissions:
            info("  " + permission)