#!/usr/bin/env python
# -*- coding:utf-8 -*-
import shutil,os

adb_path = ""

if "ANDROID_HOME" in os.environ:
    adb_path = "adb"
else:
    raise ValueError("adb命令的环境变量未设置!")
command = {
    "1" : [adb_path],
    "2" : [adb_path,"-s"],#[模拟器编号 命令]连接指定设备
    "3" : [adb_path,"version"],#adb命令版本
    "4" : [adb_path,"shell","getprop","ro.product.model"],#获取设备型号
    "5" : [adb_path,"shell","getprop","ro.build.version.sdk"],#获取SDK版本
    "6" : [adb_path,"devices"],#显示当前运行的设备
    "7" : [adb_path,"install"],#[本地apk路径]安装apk
    "8" : [adb_path,"pull"],#[<remote> <local>] 获取模拟器中的文件
    "9" : [adb_path,"push"],#[<local> <remote>]向设备中写文件
    "10": [adb_path,"uninstall"],#[apk包的主包名]卸载apk
    "11": [adb_path,"forward"],#[tcp:5555 tcp:8000] 转发PC机5555端口的数据到手机的8000端口
    "12": [adb_path,"forward","--list"],#查看转发是否成功
    "13": [adb_path,"remount"],#将system分区重新挂在为可读可写分区,此命令在操作系统目录很重要
    "14": [adb_path,"reboot"],#[bootloader|recovery],重启设备，可选参数进入bootloader刷机模式或recovery恢复模式
    "15": [adb_path,"shell","dumpsys","package"],#[<package_name>],查看app相关所有信息，包括action,codepath,version,需要的权限等等信息
    "16": [adb_path,"shell","pm","path"],#<package_name> 查看app的路径
    "17": [adb_path,"shell","dumpsys","package"],#<package_name> | grep version 查看apk的版本信息
    "18": [adb_path,"shell","dumpsys","window","|", "grep","mCurrentFocus"],#命令查看当前运行的包名和Activity
    "19": [adb_path,"shell","am","start","-n"],#<package_name>/<activity_class_name> 启动activity
    "20": [adb_path,"shell","pm","clear"],#<package_name>删除与包相关的所有数据：清除数据和缓存
    "21": [adb_path,"shell","am","start","-W"],#<package_name>/<activity_class_name>获得应用的启动时间，可以很方便地获取应用的启动时间
    "22": [adb_path,"shell","am","start","service","-n"],#<package_name>/<service_class_name>启动service
    "23": [adb_path,"shell","am","start","service","-a"],#"android.intent.action.CALL" -a后面的参数为manifest中定义的service的action
    "24": [adb_path,"shell","ps"],#<package_name|PID>查看某个app的进程相关信息
    "25": [adb_path,"shell","ps","|","grep"],#<package_name>另一种方法,查看某个app的进程相关信息
    "26": [adb_path,"shell","kill"],#pidNumber杀掉某个进程，一般用于模拟某个bug复现
    "27": [adb_path,"shell","dumpsys","meminfo"],#<package_name|PID>查看某一个app的内存占用
    "28": [adb_path,"shell","getprop","|","grep","heapgrowthlimit"],#查看单个应用程序的最大内存限制
    "29": [adb_path,"shell","dumpsys","batterystats"],#<package_name>获取单个应用的电量消耗信息
    "30": [adb_path,"shell","monkey"],#<package_name>500  -p 对象包  -v 反馈信息级别 跑monkey,测试应用的稳定性时很实用
    "31": [adb_path,"shell","cat","/system/build.prop/"],#查看设备名称
    "32": [adb_path,"shell","dumpsys","window","|","grep","Surface"],#查看手机分辨率
    "33": [adb_path,"shell","wm","size"],#查看手机分辨率
    "34": [adb_path,"shell","getprop","|","grep","build.version.release"],#查看手机sdk版本
    "35": [adb_path,"shell","getprop","|","grep","product"],#查看手机型号信息product,board,brand和cpu型号
    "36": [adb_path,"shell","cat","/sys/class/net/wlan0/address"],#查看wifi_mac,root权限
    "37": [adb_path,"shell","service","list"],#查看后台services信息
    "38": [adb_path,"shell","cat","/proc/meminfo"],#查看系统当前内存占用
    "39": [adb_path,"shell","top"],#查看设备上进程的cpu和内存占用情况
    "40": [adb_path,"logcat"],# -s 过滤指定参数log  -v time 保留日志时间>t.txt 可通过参数控制输出的日志
    "41": [adb_path,"shell","ls","-al"],#列出文件和文件夹 可选参数-al可查看文件和文件夹的详细信息
    "42": [adb_path,"shell","cd"],#folder 进入文件夹
    "43": [adb_path,"shell","cat"],#filename 查看文件
    "44": [adb_path,"shell","rename"],#path/oldfilename path/newfilename重命名文件
    "45": [adb_path,"shell","rm"],#path/filename  -r 可选参数用于删除文件夹及下面的所有文件
    "46": [adb_path,"shell","mv"],#path/filename newpath/filename 移动文件
    "47": [adb_path,"shell","cp"],#file newpath/file1 拷贝文件
    "48": [adb_path,"shell","mkdir"],#path/folder 创建目录
    "49": [adb_path,"shell","chmod"],#777 filename 设置文件最高读写权限
    "50": [adb_path,"shell","input","keyevent"],#{key_code} 发送keyevent
    "51": [adb_path,"shell","input","text"],#{text} 发送文本
    "52": [adb_path,"shell","input","touchscreen"],#swipe {x1} {y1} {x2} {y2} 滑动屏幕,xy是坐标;点击屏幕tap {x1} {y1}
    "53": [adb_path,"shell","getprop","ro.build.version.release"],#Android系统版本
    "54": [adb_path,"shell","getprop","ro.build.version.security_patch"],#Android 安全补丁程序级别
    "55": [adb_path,"shell","getprop","ro.product.model"],#设备型号
    "56": [adb_path,"shell","getprop","ro.product.brand"],#设备品牌
    "57": [adb_path,"shell","getprop","ro.product.name"],#设备名称
    "58": [adb_path,"shell","getprop","ro.product.board"],#处理器型号
    "59": [adb_path,"shell","getprop","ro.product.cpu.abilist"],#CPU支持的abi列表
    "60": [adb_path,"shell","getprop","persist.sys.isUsbOtgEnabled"],#是否支持 OTG
    "61": [adb_path,"shell","ps","|","grep","system_server"],#查看system_server进程ID

}

c_note = {
    "1": "adb命令.",
    "2": "[模拟器编号 命令] 连接指定设备.",
    "3": "adb命令的版本号.",
    "4": "获取设备型号.",
    "5": "获取SDK版本.",
    "6": "显示当前运行的设备.",
    "7": "[本地APK路径] 安装apk.",
    "8": "[<远程路径> <本地路径>] 获取模拟器中的文件.",
    "9": "[<本地路径> <远程路径>]向设备中推送文件.",
    "10": "[apk包的主包名] 卸载apk.",
    "11": "[tcp:5555 tcp:8000] 转发PC机5555端口的数据到手机的8000端口.",
    "12": "查看转发是否成功.",
    "13": "将system分区重新挂载为可读可写分区,此命令在操作系统目录很重要.",
    "14": "重启设备, [bootloader|recovery] 可选参数进入bootloader刷机模式或recovery恢复模式.",
    "15": "[<package_name>] 查看app相关所有信息，包括action,codepath,version,需要的权限等等信息.",
    "16": "<package_name> 查看app的路径",
    "17": "<package_name> | grep version 查看apk的版本信息",
    "18": "查看当前运行的包名和主Activity",
    "19": "[<package_name>/<activity_class_name>] 启动activity",
    "20": "[<package_name>] 删除与包相关的所有数据,清除数据和缓存",
    "21": "[<package_name>/<activity_class_name>] 获得应用的启动时间,可以很方便地获取应用的启动时间",
    "22": "[<package_name>/<service_class_name>] 启动service",
    "23": "[ex:android.intent.action.CALL] -a 后面的参数为manifest中service定义的action",
    "24": "[<package_name|PID>] 查看某个app的进程相关信息",
    "25": "[<package_name>] 另一种方法查看某个app的进程相关信息",
    "26": "[pid] 杀掉某个进程，一般用于模拟某个bug复现",
    "27": "[<package_name|PID>]查看某一个app的内存占用",
    "28": "查看单个应用程序的最大内存限制.",
    "29": "[<package_name>] 获取单个应用的电量消耗信息.",
    "30": "monkey测试,后接自定义参数,例如:[-p 对象包  -v 500]发送500个随机事件,测试应用的稳定性时很实用",
    "31": "查看设备名称",
    "32": "查看手机分辨率",
    "33": "另一种方法查看手机分辨率",
    "34": "另一种方法查看手机sdk版本",
    "35": "查看手机型号信息product,board,brand和cpu型号",
    "36": "查看wifi_mac,root权限",
    "37": "查看后台services信息",
    "38": "查看系统当前内存占用",
    "39": "top命令查看设备上进程的cpu和内存占用情况",
    "40": "logcat [-s 过滤指定参数log  -v time 保留日志时间>t.txt] 可通过参数控制输出的日志",
    "41": "列出文件和文件夹 可选参数-al可查看文件和文件夹的详细信息",
    "42": "[文件夹名] 进入文件夹",
    "43": "[文件名] 查看文件",
    "44": "[path/oldfilename path/newfilename] 重命名文件",
    "45": "[path/filename]  删除文件 -r 可选参数用于删除文件夹及下面的所有文件",
    "46": "[path/filename newpath/filename] 移动文件",
    "47": "[path/filename newpath/filename] 拷贝文件",
    "48": "[path/folder] 创建目录",
    "49": "[777 filename] 设置文件读写执行权限",
    "50": "[key_code] 发送按键",
    "51": "[text] 发送文本",
    "52": "[swipe x1 y1 x2 y2] 滑动屏幕,xy是坐标; [tap x1 y1] 点击屏幕",
    "53": "Android系统版本",
    "54": "Android安全补丁程序级别",
    "55": "设备型号",
    "56": "设备品牌",
    "57": "设备名称",
    "58": "处理器型号",
    "59": "CPU支持的abi列表",
    "60": "是否支持OTG",
    "61": "#查看system_server进程ID"
}