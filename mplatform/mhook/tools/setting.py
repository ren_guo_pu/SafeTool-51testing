#!/usr/bin/env python
# -*- coding:utf-8 -*-

#常用测试机配置
mobile = {
    "mumu": "adb connect 127.0.0.1:7555"
}

tapp = {
    "teching": {
        "name": "AndroGoat - Insecure App (Kotlin)",
        "package": "owasp.sat.agoat",
        "main": "owasp.sat.agoat.MainActivity"
    },
    "myapp":{
        "name": "51testingAppSafe",
        "package": "com.example.mytest",
        "main": "com.example.mytest.MainActivity"
    }
}